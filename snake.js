var canvas;
var context;
// variable position pomme
var pomme_x;
var pomme_y;
var pomme;
var ball;
// variable serpent tête et queue
var tete;
var cuerpo;
// variable de direction 
var left_D = false;
var right_D = true;
var up_D = false;
var down_D = false;
var Game = true;    // le jeu est en cours
// constantes fenetre et vitesse de jeu
const size_field = 10;
const DOTS = 900;
const rand_M = 29;
const delay = 140;
const field_height = 300;
const field_width = 300;    
// constante code ASCI button de direction clavier
const left_button = 37;
const right_button = 39;
const up_button = 38;
const down_button = 40;
// tableau posions serpent
var x = new Array(DOTS);
var y = new Array(DOTS);   

//initialisation position/images
function init() {
    
    canvas = document.getElementById('Can');
    context = canvas.getContext('2d');

    load_images();
    create_serpent();
    New_apple_position();
    setTimeout("gameCycle()", delay);
}    
// fonction load image tête/corps/pomme
function load_images() {
    
    tete = new Image();
    tete.src = 'tete.png';    
    
    ball = new Image();
    ball.src = 'corps.png'; 
    
    pomme = new Image();
    pomme.src = 'pomme.png'; 
}
// fonction nouvelle instance du serpent
function create_serpent() {

    cuerpo = 3;

    for (var z = 0; z < cuerpo; z++) {
        x[z] = 50 - z * 10;
        y[z] = 50;
    }
}
// fonction verifie rencontre serpent et pomme
function check_pomme() {

    if ((x[0] == pomme_x) && (y[0] == pomme_y)) {

        cuerpo++;
        New_apple_position();
    }
}    

function drawing() {
    
    context.clearRect(0, 0, field_width, field_height);
    
    if (Game) {

        context.drawImage(pomme, pomme_x, pomme_y);

        for (var z = 0; z < cuerpo; z++) {
            
            if (z == 0) {
                context.drawImage(tete, x[z], y[z]);
            } else {
                context.drawImage(ball, x[z], y[z]);
            }
        }    
    } else {

        gameOver();
    }        
}

function gameOver() {
    
    context.fillStyle = 'white';
    context.textBaseline = 'middle'; 
    context.textAlign = 'center'; 
    context.font = 'normal bold 18px serif';
    
    context.fillText('Game over', field_width/2, field_height/2);
}

function deplacement_serpent() {

    for (var z = cuerpo; z > 0; z--) {
        x[z] = x[(z - 1)];
        y[z] = y[(z - 1)];
    }

    if (left_D) {
        x[0] -= size_field;
    }

    if (right_D) {
        x[0] += size_field;
    }

    if (up_D) {
        y[0] -= size_field;
    }

    if (down_D) {
        y[0] += size_field;
    }
}    
// fonction set condition gameOver
function checkCollision() {

    for (var z = cuerpo; z > 0; z--) {

        if ((z > 4) && (x[0] == x[z]) && (y[0] == y[z])) {
            Game = false;
        }
    }

    if (y[0] >= field_height) {
        Game = false;
    }

    if (y[0] < 0) {
       Game = false;
    }

    if (x[0] >= field_width) {
      Game = false;
    }

    if (x[0] < 0) {
      Game = false;
    }
}

function New_apple_position() {

    var r = Math.floor(Math.random() * rand_M);
    pomme_x = r * size_field;

    r = Math.floor(Math.random() * rand_M);
    pomme_y = r * size_field;
}    

function gameCycle() {
    
    if (Game) {

        check_pomme();
        checkCollision();
        deplacement_serpent();
        drawing();
        setTimeout("gameCycle()", delay);
    }
}

onkeydown = function(e) {
    
    var key = e.keyCode;
    
    if ((key == left_button) && (!right_D)) {
        
        left_D = true;
        up_D = false;
        down_D = false;
    }

    if ((key == right_button) && (!left_D)) {
        
        right_D = true;
        up_D = false;
        down_D = false;
    }

    if ((key == up_button) && (!down_D)) {
        
        up_D = true;
        right_D = false;
        left_D = false;
    }

    if ((key == down_button) && (!up_D)) {
        
        down_D = true;
        right_D = false;
        left_D = false;
    }        
};    
